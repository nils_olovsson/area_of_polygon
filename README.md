**Area of polygon**
A python implementation of the method for calculating the area of a polygon
in 2D based on Green's theorem.

Running the script will create the figures used in the blog post.

Blog post:
http://www.all-systems-phenomenal.com/articles/area_of_the_polygon_and_greens_theorem

**License: MIT**
