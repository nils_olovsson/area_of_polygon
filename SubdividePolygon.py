#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import numpy as np

def hermite(p0, t0, p1, t1, t):
    """
        Hermite spline interpolation between two points with tangents.
    """
    h0 = 1 - 3.0*t*t + 2.0*t*t*t;
    h1 = t - 2.0*t*t + 1.0*t*t*t;
    h2 =        -t*t +     t*t*t;
    h3 =     3.0*t*t - 2.0*t*t*t;
    return h0*p0 + h1*t0 + h2*t1 + h3*p1

def subdividePolygon(vertices, tangents, s, weight=1.0):
    """
        Subdive and interpolate polygon using hermite spline.
        An extra 's' number of points are added between each pair of points
        in the original polygon.
    """
    shape = np.shape(vertices)

    n = shape[0]
    m = (s + 1) * n
    points = np.zeros([m, 2])

    ind = 0;
    for i in range(0, n):
        k = (i + 1) % n
        j = i - 1
        if j<0:
            j = n -1 - i

        p0 = vertices[i, :]
        p1 = vertices[k, :]
        t0 = tangents[i, :]
        t1 = tangents[k, :]

        pj = vertices[j,:]
        t0 = p0 - pj
        t1 = p1 - p0

        l0 = np.linalg.norm(t0)
        l1 = np.linalg.norm(t1)

        l = min(l0, l1)

        t0 *= 1.0/l
        t1 *= 1.0/l
        t0 *= weight
        t1 *= weight

        points[ind, :] = p0
        ind += 1

        dt = 1.0 / (s + 1)
        t = 0.0
        for j in range(0, s):
            t += dt
            points[ind, :] = hermite(p0, t0, p1, t1, t)
            ind += 1
    return points
