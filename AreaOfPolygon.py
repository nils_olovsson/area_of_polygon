#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

def computeAreaOfPolygon(vertices):
    """
        Compute area of a polygon in 2D.
        Assumption that the polygon is simple, i.e is closed and has no
        crossings and also that its vertex order is counter clockwise.
    """
    area = 0.0
    n = len(vertices)
    for i in range(n):
        j = i + 1
        j %= n
        p0 = vertices[i]
        p1 = vertices[j]
        area += p1[1]*p0[0] - p0[1]*p1[0]
    area *= 0.5
    return area
