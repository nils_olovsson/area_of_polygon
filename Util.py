#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import os

def mkdir(dirs):
    """
        If directories and subdirectories do not exist, create them.
        dirs is a list of subdirectory names,
        mkdir(['foo', 'bar']) -> /foo/bar/
    """
    directory = ''
    for d in dirs:
        directory = os.path.join(directory, d)
        if not os.path.isdir(directory):
            os.mkdir(directory, 0o0755)
    return directory
