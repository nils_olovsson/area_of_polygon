#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import numpy as np

import ComputePolygon

# ==============================================================================
#
# Generate Polygon
#
# ==============================================================================

def simple():
    """
        Generate a simply bounded polygon
    """

    vertices = np.zeros([15, 2])
    vertices[0,:] = np.array([0.10, 0.20])
    vertices[1,:] = np.array([0.20, 0.10])
    vertices[2,:] = np.array([0.30, 0.15])
    vertices[3,:] = np.array([0.50, 0.05])
    vertices[4,:] = np.array([0.70, 0.30])
    vertices[5,:] = np.array([0.60, 0.40])
    vertices[6,:] = np.array([0.50, 0.35])

    vertices[7,:]  = np.array([0.45, 0.50])
    vertices[8,:]  = np.array([0.40, 0.40])
    vertices[9,:]  = np.array([0.35, 0.47])
    vertices[10,:] = np.array([0.42, 0.60])
    vertices[11,:] = np.array([0.37, 0.65])
    vertices[12,:] = np.array([0.30, 0.55])
    vertices[13,:] = np.array([0.25, 0.35])
    vertices[14,:] = np.array([0.15, 0.30])

    # Scale the vertex positions
    xmin = np.amin(vertices[:, 0])
    xmax = np.amax(vertices[:, 0])
    ymin = np.amin(vertices[:, 1])
    ymax = np.amax(vertices[:, 1])
    xscale = 1.5 / (xmax - xmin)
    yscale = 1.0 / (ymax - ymin)
    vertices[:, 0] = xscale * vertices[:, 0]
    vertices[:, 1] = yscale * vertices[:, 1]

    normals = ComputePolygon.normals(vertices)
    return vertices, normals

def simpleAlt():
    """
        Generate a simply bounded polygon
    """

    vertices = np.zeros([7, 2])
    vertices[0,:] = np.array([0.00, 0.05])
    vertices[1,:] = np.array([0.30, 0.09])
    vertices[2,:] = np.array([0.40, 0.20])
    vertices[3,:] = np.array([0.60, 0.00])
    vertices[4,:] = np.array([0.50, 0.40])
    vertices[5,:] = np.array([0.25, 0.095])
    vertices[6,:] = np.array([0.10, 0.30])

    n = len(vertices)
    for i in range(0, n):
        vertices[i, 0] = 2*(1.0/0.6)*vertices[i, 0]
        vertices[i, 1] = (1.0/0.4)*vertices[i, 1]
    normals = ComputePolygon.normals(vertices)
    return vertices, normals

def box():
    """
        A square box.
    """
    vertices = np.zeros([4, 2])
    vertices[0,:] = np.array([-0.5, -0.5])
    vertices[1,:] = np.array([ 0.5, -0.5])
    vertices[2,:] = np.array([ 0.5,  0.5])
    vertices[3,:] = np.array([-0.5,  0.5])
    normals = ComputePolygon.normals(vertices)
    return vertices, normals

def circle(n=64, CCW=True):
    """
        A round (or not) circle.
    """
    r = 1
    vertices = np.zeros([n,2])
    normals  = np.zeros([n,2])
    indices  = np.zeros([n,3], dtype=np.int)
    a = 0.0
    for i in range(n):
        vertices[i,0] = 0.5*np.cos(a)
        vertices[i,1] = 0.5*np.sin(a)
        normals[i,0] = np.cos(a)
        normals[i,1] = np.sin(a)
        if CCW:
            a += 2*np.pi / n
        else:
            a -= 2*np.pi / n
    return vertices, normals

def kochSnowflake(order=3, scale=0.5):
    """
    Return nx2 matrix of point coordinates of the Koch snowflake.
    https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/fill.html

    Arguments
    ---------
    order : int
        The recursion depth.
    scale : float
        The extent of the snowflake (edge length of the base triangle).
    """
    def _koch_snowflake_complex(order):
        if order == 0:
            # initial triangle
            angles = np.array([0, 120, 240]) + 90
            return scale / np.sqrt(3) * np.exp(np.deg2rad(angles) * 1j)
        else:
            ZR = 0.5 - 0.5j * np.sqrt(3) / 3

            p1 = _koch_snowflake_complex(order - 1)  # start points
            p2 = np.roll(p1, shift=-1)  # end points
            dp = p2 - p1  # connection vectors

            new_points = np.empty(len(p1) * 4, dtype=np.complex128)
            new_points[::4] = p1
            new_points[1::4] = p1 + dp / 3
            new_points[2::4] = p1 + dp * ZR
            new_points[3::4] = p1 + dp / 3 * 2
            return new_points
    points = _koch_snowflake_complex(order)
    vertices = np.array([points.real, points.imag])
    vertices = vertices.transpose()
    shape = np.shape(vertices)
    normals = np.zeros(shape)
    return vertices, normals
