#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2021-03-27
license: MIT

"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

import GeneratePolygon
import ComputePolygon
import SubdividePolygon
import DrawPolygon
import Util

from AreaOfPolygon import computeAreaOfPolygon

def drawIntroShape():
    """
        Create figure used in introduction.
    """

    options = DrawPolygon.defaultOptions()

    p, n, = GeneratePolygon.simpleAlt()

    t = ComputePolygon.tangents(p)
    p = SubdividePolygon.subdividePolygon(p, t, 40, 0.2)
    n = ComputePolygon.normals(p)

    color = '#CCCCFF'
    fig, lines = DrawPolygon.filled(p, n, color,
                                    arrow_positions=[],
                                    figsize=[12, 6],
                                    options=options)
    filepath = './img/intro_shape.svg'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    plt.close()

def drawDomain():
    """
        Smooth domain with orientation, tangent and normal.
    """

    options = DrawPolygon.defaultOptions()

    p, n, = GeneratePolygon.simple()

    t = ComputePolygon.tangents(p)
    p = SubdividePolygon.subdividePolygon(p, t, 40, 0.2, )

    color = '#CCCCFF'

    arrow_positions = []
    arrow_positions.append(int(0.05*len(p)))
    arrow_positions.append(int(0.19*len(p)))
    arrow_positions.append(int(0.30*len(p)))
    arrow_positions.append(int(0.80*len(p)))
    fig, lines = DrawPolygon.filled(p, n, color,
                                    arrow_positions=arrow_positions,
                                    figsize=[15, 10],
                                    options=options)
    matplotlib.rcParams['text.usetex'] = True

    i = int(0.46*len(p))
    DrawPolygon.drawNormalAndTangent(p, i, fig, options)

    plt.text(1.2, 0.4, r'$\Omega$', fontsize=64)
    plt.text(1.5, 0.2, r'$\partial \Omega$', fontsize=64)

    filepath = './img/oriented_domain.svg'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    plt.close()

def drawDomainHole():
    """
        Smooth domain with a circular hole.
    """

    options = DrawPolygon.defaultOptions()

    p0, n0, = GeneratePolygon.simple()
    p1, n1, = GeneratePolygon.circle(CCW=False)
    p1 = 0.35 * p1
    p1 += np.array([1.2, 0.35])

    t = ComputePolygon.tangents(p0)
    p0 = SubdividePolygon.subdividePolygon(p0, t, 40, 0.2)

    color = '#CCCCFF'

    arrow_positions = []
    arrow_positions.append(int(0.05*len(p0)))
    arrow_positions.append(int(0.19*len(p0)))
    arrow_positions.append(int(0.30*len(p0)))
    arrow_positions.append(int(0.80*len(p0)))
    fig, lines = DrawPolygon.filled(p0, n0, color,
                                    arrow_positions=arrow_positions,
                                    figsize=[15, 10],
                                    options=options)
    options['z-offset'] = 5
    fig, lines = DrawPolygon.filled(p1, n1, '#FFFFFF',
                                    arrow_positions=[0, 16, 32, 48],
                                    fig=fig,
                                    options=options)
    filepath = './img/domain_with_hole.svg'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    filepath = './img/domain_with_hole.png'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    plt.close()

def drawPolygonWithVertices():
    """
        Draw example polygon
    """

    options = DrawPolygon.defaultOptions()
    options['annotate-verts'] = True
    options['draw-vertices'] = True

    p, n, = GeneratePolygon.simple()

    color = '#CCCCFF'
    fig, lines = DrawPolygon.filled(p, n, color,
                                    figsize=[15, 10],
                                    options=options)
    filepath = './img/polygon.svg'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    plt.close()

def drawColorMappedShapes():
    """
        Draw a few shapes that are colormapped to their area.
    """
    options = DrawPolygon.defaultOptions()
    color = '#CCCCFF'

    shapes = []
    n = 8

    # Offsets to place polygons on 2x4 "grid" when plotting
    offset = np.zeros([2,8])
    offset[:,0] = np.array([0.0,  0.0])
    offset[:,1] = np.array([1.0,  0.0])
    offset[:,2] = np.array([2.0,  0.0])
    offset[:,3] = np.array([3.0,  0.0])
    offset[:,4] = np.array([0.0, -1.0])
    offset[:,5] = np.array([1.0, -1.0])
    offset[:,6] = np.array([2.0, -1.0])
    offset[:,7] = np.array([3.0, -1.0])

    fig = plt.figure(figsize=(40, 16))
    for i in range(n-3):
        p, _ = GeneratePolygon.kochSnowflake(order=i)
        p += offset[:,i]
        shapes.append(p)

    p, _ = GeneratePolygon.circle(8)
    p *= 0.5
    p += offset[:,5]
    shapes.append(p)

    p, _ = GeneratePolygon.circle(128)
    p *= 0.5
    p += offset[:,6]
    shapes.append(p)

    p, _ = GeneratePolygon.box()
    p *= 0.5
    p += offset[:,7]
    shapes.append(p)

    areas = []
    for polygon in shapes:
        area = computeAreaOfPolygon(polygon)
        areas.append(area)
    areas = np.array(areas)
    amin = np.amin(areas)
    amax = np.amax(areas)
    areas -= amin
    areas /= (amax - amin)

    cmap = matplotlib.cm.get_cmap('viridis')

    for i, polygon in enumerate(shapes):
        rgba = cmap(areas[i])
        _, _ = DrawPolygon.filled(polygon, None, rgba, fig=fig, options=options)

    #allverts = np.concatenate(shapes, axis=0)
    #xmin = np.amin(allverts[:,0])
    #xmax = np.amax(allverts[:,0])
    #ymin = np.amin(allverts[:,1])
    #ymax = np.amax(allverts[:,1])

    ax = fig.gca()
    ax.set_ylim([-1.3, 0.3])
    ax.set_xlim([-0.5, 3.5])

    filepath = './img/areas.svg'
    plt.savefig(filepath, bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    print("Saved figure to '{}'".format(filepath))
    plt.close()

# ==============================================================================
#
# Main
#
# ==============================================================================

if __name__=='__main__':
    print("Will make figures.")
    Util.mkdir(['./',  'img'])
    drawIntroShape()
    drawDomain()
    drawPolygonWithVertices()
    drawDomainHole()
    drawColorMappedShapes()
    print("Done!")
